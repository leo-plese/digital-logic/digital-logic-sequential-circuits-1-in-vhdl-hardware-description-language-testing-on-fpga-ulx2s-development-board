library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity broj is
    port (
	clk_25m: in std_logic;
	btn_down: in std_logic;
	btn_right: in std_logic;
	sw: in std_logic_vector(3 downto 0);
	led: out std_logic_vector(7 downto 0)
    );
end broj;

architecture x of broj is
    signal R: std_logic_vector(7 downto 0);
    signal delta: std_logic_vector(7 downto 0) := "00000001";
    signal clk: std_logic;

begin
 led <= R;
 process(clk)
 begin

	if (rising_edge(clk)) then
		if (btn_right='1') then 
			R <= "00000000";
		else
			if (R = "01000101") then
				R <= "00000000";
			else
				R <= (R + delta);
			end if;
		end if;
	end if;
 end process;
 
 
 I_upravljac: entity upravljac
 port map (
 clk_25m => clk_25m, Clk_key => btn_down, Clk => clk,
 AddrA_key => '0', AddrB_key => '0', ALUOp_key => '0',
 AddrA => open, AddrB => open, ALUOp => open
 );
end x;
